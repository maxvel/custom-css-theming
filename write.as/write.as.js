/* Custom Footer*/
var customFooterHTML = `
  <div id="customFooter">
      <div class="footericons">
        <a href="https://pixelfed.social/maxvel">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Pixelfed_Logo.svg/2048px-Pixelfed_Logo.svg.png" class="icon"> 
        </a>
        
        <a href="https://fosstodon.org/@maxvel">
          <img src="https://static-00.iconduck.com/assets.00/mastodon-icon-512x512-fl0fay7j.png" class="icon"> 
        </a>
        
        <a href="https://codeberg.org/maxvel">
          <img src="https://codeberg.org/img/codeberg.png" class="icon"> 
        </a>
        
        <a href="https://maxvel@nerdsin.space">
          <img src="https://element.io/images/logo-mark-primary.svg" class="icon"> 
        </a>
        
        <a href="https://maxvel@404.city">
          <img src="https://conversations.im/images/conversations_logo.png" class="icon"> 
        </a>
        </div>
        <div class="disclaimer">
            Copyright © 2019 - 2021 by Maxvel <br><br>
            All the posts are published under the licence CC BY SA 4.0 unless otherwise stated. <br><br>
            The opinions expressed herein are my own and do not represent those of my employer or any other third-party views in any way. <br><br>
            Proudly Hosted with Write.as Pro<br><br>
        </div>
    </div>
`


var x = document.querySelector('footer').getElementsByTagName('nav')[0];
x.innerHTML = customFooterHTML